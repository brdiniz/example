FactoryGirl.define do
  factory :message do
    to_id { create(:user).id }
    subject { FFaker::CheesyLingo::title }
    body { FFaker::CheesyLingo::sentence }
  end
end
