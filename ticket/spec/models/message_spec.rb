require 'rails_helper'

RSpec.describe Message, type: :model do
  context "associations" do
    it { is_expected.to belong_to(:from) }
    it { is_expected.to belong_to(:to) }
  end
end
