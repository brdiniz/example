require 'rails_helper'

RSpec.describe User, type: :model do
  context "associations" do
    it { is_expected.to have_many(:inbox) }
    it { is_expected.to have_many(:sends) }
  end
end
