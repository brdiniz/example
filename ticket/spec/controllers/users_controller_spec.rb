require 'rails_helper'

RSpec.describe UsersController do
  let(:admin) { create(:user, admin: true) }
  let(:user) { create(:user) }

  describe ".index" do
    context "when User is not admin" do
      before do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        sign_in user

        process :index, method: :get
      end

      context "when user redirect to" do
        subject { response }

        it { is_expected.to redirect_to messages_path }
      end

      context "when user flash is" do
        subject { flash[:notice] }

        it { is_expected.to eq("You don't have access") }
      end
    end

    context "when User is admin" do
      before do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        sign_in admin
        create_list(:user, 10)

        process :index, method: :get
      end

      context "when user list is" do
        subject { assigns[:users].count }

        it { is_expected.to be(11) }
      end
    end
  end

  describe ".destroy" do
    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in admin

      process :destroy, method: :delete, params: { id: user }
    end

    context "when user deleted redirect to" do
      subject { response }

      it { is_expected.to redirect_to users_path }
    end

    context "when user deleted flash is" do
      subject { flash[:notice] }

      it { is_expected.to eq("User deleted.") }
    end
  end

  describe ".update" do
    let(:user_params) do
      {
        password: "kijuhygt",
        admin: true
      }
    end

    before do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      sign_in admin

      process :update, method: :put, params: { user: user_params, id: user }
    end

    context "when redirect to" do
      subject { response }

      it { is_expected.to redirect_to users_path }
    end
  end
end
