require 'rails_helper'

RSpec.describe MessagesController do
  let(:user) { create(:user) }

  before do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    sign_in user
  end

  describe ".index" do
    before do
      create_list(:user, 10)
      process :index, method: :get
    end

    context "when users list is" do
      subject { assigns[:users].include? user }

      it { is_expected.to be_falsey }
    end

    context "when message is" do
      subject { assigns[:message] }

      it { is_expected.to_not be_nil }
    end
  end

  describe ".create" do
    let(:message_params) { attributes_for(:message) }
    let(:to_email) { User.find(message_params[:to_id]).email }
    let(:mock_response_body) do
      {
        "from"=>"#{user.email} <postmaster@sandbox11b4809092fe470488c5cd1324599ca2.mailgun.org>",
        "subject"=> message_params[:subject],
        "text"=> message_params[:body],
        "to"=> to_email
      }
    end
    let(:mock_response_headers) do
      {
        'Accept'=>'*/*',
        'Accept-Encoding'=>'gzip, deflate',
        'Authorization'=>'Basic YXBpOmtleS04MmEwYTUyY2JlZTQ3NDNiMTA2NGU4YWQ3MTNiODFmNQ==',
        'Content-Length'=>/.+/,
        'Content-Type'=>'application/x-www-form-urlencoded',
        'Host'=>'api.mailgun.net',
        'User-Agent'=>/.+/
      }
    end
    let(:post_to_api_mailgun) {
      "https://api.mailgun.net/v3/sandbox11b4809092fe470488c5cd1324599ca2.mailgun.org/messages"
    }

    before do
      stub_request(:post, post_to_api_mailgun).
       with(:body => mock_response_body,
            :headers => mock_response_headers
      ).to_return(:status => 200, :body => "", :headers => {})

      process :create, method: :post, params: { message: message_params }
    end

    context "when redirect to" do
      subject { response }

      it { is_expected.to redirect_to messages_path }
    end

    context "when flash message is" do
      subject { flash[:notice] }

      it { is_expected.to eq("Message created.") }
    end
  end
end
