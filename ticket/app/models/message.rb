class Message < ApplicationRecord
  belongs_to :from, class_name: "User", foreign_key: :from_id
  belongs_to :to, class_name: "User", foreign_key: :to_id

  after_create :send_simple_message

  private
    def send_simple_message
      # Call api mailgun to send email.
      # The call is executed when after a message is created.
      RestClient.post "https://api:key-82a0a52cbee4743b1064e8ad713b81f5"\
        "@api.mailgun.net/v3/sandbox11b4809092fe470488c5cd1324599ca2.mailgun.org/messages",
        from: "#{self.from.email} <postmaster@sandbox11b4809092fe470488c5cd1324599ca2.mailgun.org>",
        to: self.to.email,
        subject: self.subject,
        text: self.body
    end
end
