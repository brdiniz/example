class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :inbox, class_name: "Message", foreign_key: :to_id
  has_many :sends, class_name: "Message", foreign_key: :from_id
end
