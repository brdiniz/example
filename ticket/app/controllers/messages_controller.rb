class MessagesController < ApplicationController
  # devise method to verified user connected.
  before_action :authenticate_user!

  def create
    @message = Message.new(message_params)
    @message.from = current_user
    if @message.save
      return redirect_to messages_path, notice: 'Message created.'
    else
      render :index
    end
  end

  def index
    @message = Message.new
    @users = User.where.not(id: current_user.id).order(:email)
  end

  private
    def message_params
      params.require(:message)
             .permit(:to_id, :subject, :body)
    end
end
