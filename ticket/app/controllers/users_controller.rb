class UsersController < ApplicationController
  # devise method to verified user connected.
  before_action :authenticate_user!
  # call method to verify when user is admin
  before_action :verify_admin
  # call method to find user when params id is present.
  before_action :find_user

  def update
    if @user.update(user_params)
      redirect_to users_path, notice: 'User updated.'
    else
      render :edit
    end
  end

  def index
    @users = User.all
  end

  def destroy
    if @user.delete
      redirect_to users_path, notice: 'User deleted.'
    end
  end

  private
    def verify_admin
      return if current_user.admin
      redirect_to messages_path, notice: "You don't have access"
    end

    def find_user
      @user = User.find params[:id] if params[:id]
    end

    def user_params
      params.require(:user)
            .permit(:email, :password, :admin)
    end
end
