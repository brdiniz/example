require 'spec_helper'

RSpec.describe Suit do
  let(:suit_params) do
    {
      name: 'Spades',
      id: 1,
      special: false
    }
  end

  let(:suit) { Suit.new(suit_params) }

  describe '.id' do
    subject { suit.id }

    it { is_expected.to eq(suit_params[:id]) }
  end

  describe '.name' do
    subject { suit.name }

    it { is_expected.to eq(suit_params[:name]) }
  end

  describe '.special' do
    subject { suit.special }

    it { is_expected.to eq(suit_params[:special]) }
  end
end
