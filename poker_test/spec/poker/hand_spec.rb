require 'spec_helper'

RSpec.describe Hand do
  let(:suit_cubs) { Suit.new(id: 1, name: 'cubs', special: false) }
  let(:suit_spades) { Suit.new(id: 2, name: 'spades', special: false) }
  let(:suit_diamonds) { Suit.new(id: 3, name: 'diamonds', special: false) }
  let(:suit_hearts) { Suit.new(id: 4, name: 'hearts', special: true) }

  describe '.ranking for high_card' do
    let(:cards) do
      [
        Card.new(name: 'K', suit: suit_hearts, power: '13'),
        Card.new(name: '7', suit: suit_diamonds, power: '7'),
        Card.new(name: '5', suit: suit_hearts, power: '5'),
        Card.new(name: '3', suit: suit_spades, power: '3'),
        Card.new(name: '2', suit: suit_diamonds, power: '2'),
      ]
    end

    let(:hand) { Hand.new(cards: cards) }

    subject { hand.ranking }

    it { is_expected.to eq('High card') }
  end

  describe '.ranking for one_pair' do
    let(:cards) do
      [
        Card.new(name: 'A', suit: suit_spades, power: '14'),
        Card.new(name: 'A', suit: suit_diamonds, power: '14'),
        Card.new(name: '3', suit: suit_spades, power: '3'),
        Card.new(name: '8', suit: suit_diamonds, power: '8'),
        Card.new(name: '10', suit: suit_hearts, power: '10'),
      ]
    end

    let(:hand) { Hand.new(cards: cards) }

    subject { hand.ranking }

    it { is_expected.to eq('One pair') }
  end

  describe '.ranking for two_pairs' do
    let(:cards) do
      [
        Card.new(name: 'A', suit: suit_spades, power: '14'),
        Card.new(name: 'A', suit: suit_diamonds, power: '14'),
        Card.new(name: '3', suit: suit_spades, power: '3'),
        Card.new(name: '3', suit: suit_diamonds, power: '3'),
        Card.new(name: '10', suit: suit_hearts, power: '10'),
      ]
    end

    let(:hand) { Hand.new(cards: cards) }

    subject { hand.ranking }

    it { is_expected.to eq('Two pairs') }
  end

  describe '.ranking for two_pairs' do
    let(:cards) do
      [
        Card.new(name: 'A', suit: suit_spades, power: '14'),
        Card.new(name: 'A', suit: suit_diamonds, power: '14'),
        Card.new(name: '3', suit: suit_spades, power: '3'),
        Card.new(name: '3', suit: suit_diamonds, power: '3'),
        Card.new(name: '10', suit: suit_hearts, power: '10'),
      ]
    end

    let(:hand) { Hand.new(cards: cards) }

    subject { hand.ranking }

    it { is_expected.to eq('Two pairs') }
  end

  describe '.ranking for three_of_a_kind' do
    let(:cards) do
      [
        Card.new(name: 'J', suit: suit_spades, power: '11'),
        Card.new(name: 'J', suit: suit_diamonds, power: '11'),
        Card.new(name: 'J', suit: suit_hearts, power: '11'),
        Card.new(name: '3', suit: suit_diamonds, power: '3'),
        Card.new(name: '10', suit: suit_hearts, power: '10'),
      ]
    end

    let(:hand) { Hand.new(cards: cards) }

    subject { hand.ranking }

    it { is_expected.to eq('Three of a Kind') }
  end

  describe '.ranking for straight' do
    let(:cards) do
      [
        Card.new(name: '9', suit: suit_hearts, power: '9'),
        Card.new(name: '8', suit: suit_diamonds, power: '8'),
        Card.new(name: '7', suit: suit_spades, power: '7'),
        Card.new(name: '6', suit: suit_spades, power: '6'),
        Card.new(name: '5', suit: suit_hearts, power: '5')
      ]
    end

    let(:hand) { Hand.new(cards: cards) }

    subject { hand.ranking }

    it { is_expected.to eq('Straight') }
  end

  describe '.ranking for flush' do
    let(:cards) do
      [
        Card.new(name: 'K', suit: suit_diamonds, power: '13'),
        Card.new(name: 'Q', suit: suit_diamonds, power: '12'),
        Card.new(name: '9', suit: suit_diamonds, power: '9'),
        Card.new(name: '8', suit: suit_diamonds, power: '8'),
        Card.new(name: '5', suit: suit_diamonds, power: '5')
      ]
    end

    let(:hand) { Hand.new(cards: cards) }

    subject { hand.ranking }

    it { is_expected.to eq('Flush') }
  end

  describe '.ranking for Full House' do
    let(:cards) do
      [
        Card.new(name: 'K', suit: suit_cubs, power: '11'),
        Card.new(name: 'K', suit: suit_diamonds, power: '11'),
        Card.new(name: '8', suit: suit_diamonds, power: '8'),
        Card.new(name: '8', suit: suit_spades, power: '8'),
        Card.new(name: '8', suit: suit_hearts, power: '8')
      ]
    end

    let(:hand) { Hand.new(cards: cards) }

    subject { hand.ranking }

    it { is_expected.to eq('Full House') }
  end

  describe '.ranking for four_of_a_kind' do
    let(:cards) do
      [
        Card.new(name: '8', suit: suit_cubs, power: '8'),
        Card.new(name: 'K', suit: suit_diamonds, power: '11'),
        Card.new(name: '8', suit: suit_diamonds, power: '8'),
        Card.new(name: '8', suit: suit_spades, power: '8'),
        Card.new(name: '8', suit: suit_hearts, power: '8')
      ]
    end

    let(:hand) { Hand.new(cards: cards) }

    subject { hand.ranking }

    it { is_expected.to eq('Four of a Kind') }
  end

  describe '.ranking for straight_flush' do
    let(:cards) do
      [
        Card.new(name: '10', suit: suit_hearts, power: '10'),
        Card.new(name: '9', suit: suit_hearts, power: '9'),
        Card.new(name: '8', suit: suit_hearts, power: '8'),
        Card.new(name: '7', suit: suit_hearts, power: '7'),
        Card.new(name: '6', suit: suit_hearts, power: '6')
      ]
    end

    let(:hand) { Hand.new(cards: cards) }

    subject { hand.ranking }

    it { is_expected.to eq('Straight Flush') }
  end
end
