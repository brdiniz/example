require 'spec_helper'

RSpec.describe Card do
  let(:suit) do
    Suit.new(id: 2, name: 'diamonds')
  end

  let(:card_params) do
    {
      name: 'A',
      suit: suit,
      power: 14
    }
  end

  let(:card) { Card.new(card_params) }

  describe '.name' do
    subject { card.name }

    it { is_expected.to eq(card_params[:name]) }
  end

  describe '.suit' do
    subject { card.suit }

    it { is_expected.to eq(suit) }
  end

  describe '.power' do
    subject { card.power }

    it { is_expected.to eq(card_params[:power]) }
  end

  describe '.description' do
    subject { card.description }

    it { is_expected.to eq("#{card.name} #{card.suit.name}") }
  end
end
