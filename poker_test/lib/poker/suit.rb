class Suit
  attr_accessor :name
  attr_accessor :id
  attr_accessor :special

  def initialize(params)
    self.name = params[:name]
    self.id = params[:id]
    self.special = params[:special]
  end
end
