class Card
  attr_accessor :name
  attr_accessor :suit
  attr_accessor :power

  def initialize(params)
    self.name = params[:name]
    self.suit = params[:suit]
    self.power = params[:power].to_i
  end

  def description
    "#{name} #{suit.name}"
  end
end
