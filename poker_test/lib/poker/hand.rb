class Hand
  attr_accessor :cards

  def initialize(params)
    self.cards = params[:cards].sample(5)
  end

  def ranking
    return 'Straight Flush' if straight_flush
    return 'Four of a Kind' if four_of_a_kind
    return 'Full House' if full_house
    return 'Flush' if flush
    return 'Straight' if straight
    return 'Three of a Kind' if three_of_a_kind
    return 'Two pairs' if two_pairs
    return 'One pair' if one_pair
    'High card'
  end

  private

  def straight_flush
    return false unless five_sequences_of_cards
    return false unless cards.map(&:suit).uniq.count == 1
    return cards.map(&:suit).uniq.first.special
  end

  def four_of_a_kind
    return check_power_into_card(4, 1)
  end

  def full_house
    return check_power_into_card(3, 1) &&
            check_power_into_card(2, 1)
  end

  def flush
    return false if five_sequences_of_cards
    return cards.map(&:suit).uniq.count == 1
  end

  def straight
    five_sequences_of_cards
  end

  def three_of_a_kind
    return check_power_into_card(3, 1)
  end

  def two_pairs
    return check_power_into_card(2, 2)
  end

  def one_pair
    return check_power_into_card(2, 1)
  end

  def five_sequences_of_cards
    return false unless cards.count == 5
    powers = cards.sort_by { |c| c.power }.map(&:power)
    powers.each_cons(2).all? {|a, b| b == a + 1 }
  end

  def check_power_into_card(card_repeat, card_count)
    pairs = Hash.new(0)

    cards.each do |v|
      pairs[v.power] += 1
    end

    return pairs.select { |b,v| v == card_repeat }.count == card_count
  end
end
