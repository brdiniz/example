require './lib/poker.rb'

suits = [
  Suit.new(id: 1, name: 'cubs', special: false),
  Suit.new(id: 2, name: 'spades', special: false),
  Suit.new(id: 3, name: 'diamonds', special: false),
  Suit.new(id: 4, name: 'hearts', special: true)
]

card_params = []

(2..10).each { |i| card_params << { power: i, name: i.to_s } }

card_params << { power: 11, name: 'Jack' }
card_params << { power: 12, name: 'Queen' }
card_params << { power: 13, name: 'King' }
card_params << { power: 14, name: 'Ace' }

cards = []

suits.each do |suit|
  card_params.each do |card_attributes|
    cards << Card.new(card_attributes.merge!(suit: suit))
  end
end

hand = Hand.new(cards: cards)

puts 'Random Hand: ' + hand.cards.map(&:description).join(", ")
puts 'Ranking: ' + hand.ranking
